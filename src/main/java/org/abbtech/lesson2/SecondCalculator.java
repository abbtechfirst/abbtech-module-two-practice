package org.abbtech.lesson2;

public class SecondCalculator {
    public int addition(int a, int b){
        return a+b;
    }
    public int substraction(int a, int b){
        return a-b;
    }
    public int multiply(int a, int b){
        return a*b;
    }
    public double division(int a, int b){
        if (b==0) {
            throw new ArithmeticException("you cant divide to zero");
        }
        return (double) a/b;
    }
}
