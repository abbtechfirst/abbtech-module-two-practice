package org.abbtech.lesson1;

//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
public class Main {
    public static void main(String[] args) {
        Calculator calculator=new Calculator();
        System.out.println(calculator.add(2,4));
        System.out.println(calculator.multiply(2,4));
    }
}