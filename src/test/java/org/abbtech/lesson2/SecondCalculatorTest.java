package org.abbtech.lesson2;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class SecondCalculatorTest {
    SecondCalculator calculator;
    @BeforeEach void init(){
        calculator=new SecondCalculator();
    }
    @Test void additionTest(){
        int result=calculator.addition(2,5);
        Assertions.assertEquals(result,7);
    }

    @Test void multiplyTest(){
        int result=calculator.multiply(2,5);
        Assertions.assertSame(result,10);
    }
    @Test void substractTest(){
        int result=calculator.substraction(2,5);
        Assertions.assertSame(result,-3);
    }
    @Test void divisionTest() {
        Assertions.assertThrows(ArithmeticException.class, () -> {
            calculator.division(2, 0);
        });

    }}
